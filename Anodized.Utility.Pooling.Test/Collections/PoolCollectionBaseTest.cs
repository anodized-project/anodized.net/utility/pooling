using System;
using System.Collections.Generic;
using System.Text;

using Anodized.Utility.Pooling.Collections;

using FluentAssertions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Anodized.Utility.Pooling.Test.Collections
{
    [TestClass]
    public class PoolCollectionBaseTest
    {
        [TestMethod]
        public void TestCreate()
        {
            var collection = this.Invoking(t => t.CreateMockCollection()).Should().NotThrow().Subject;

            Assert.IsNotNull(collection);

            const int size = PoolCollectionBase<MockClass>.MIN_STACK_SIZE;

            collection.GetCurrentStackSize().Should().Be(size);
            collection.GetObjectsInStack().Should().Be(size);
            collection.GetObjectsInUse().Should().Be(0);
        }

        [TestMethod]
        public void TestTakeReturnOnce()
        {
            var collection = CreateMockCollection();

            var obj = collection.Invoking(c => c.Take()).Should().NotThrow().Subject;

            const int size = PoolCollectionBase<MockClass>.MIN_STACK_SIZE;

            collection.GetCurrentStackSize().Should().Be(size);
            collection.GetObjectsInStack().Should().Be(size - 1);
            collection.GetObjectsInUse().Should().Be(1);

            collection.Invoking(c => c.Return(ref obj)).Should().NotThrow();

            Assert.IsNull(obj);

            collection.GetCurrentStackSize().Should().Be(size);
            collection.GetObjectsInStack().Should().Be(size);
            collection.GetObjectsInUse().Should().Be(0);
        }

        [TestMethod]
        public void TestTakeReturnBatch()
        {
            const int batchSize = 100;

            var collection = CreateMockCollection();

            List<MockClass> objects = new List<MockClass>();

            for (int i = 0; i < batchSize; i++)
            {
                var obj = collection.Take();

                Assert.IsNotNull(obj);

                objects.Add(obj);
            }

            const int expectedStackSize = 112;
            const int expectedObjInUse = batchSize;
            const int expectedObjInStack = 12;

            collection.GetCurrentStackSize().Should().Be(expectedStackSize);
            collection.GetObjectsInStack().Should().Be(expectedObjInStack);
            collection.GetObjectsInUse().Should().Be(expectedObjInUse);

            for (int i = batchSize - 1; i > -1; i--)
            {
                var obj = objects[i];

                objects.Remove(obj).Should().BeTrue();

                collection.Return(ref obj);

                Assert.IsNull(obj);
            }

            collection.GetCurrentStackSize().Should().Be(expectedStackSize);
            collection.GetObjectsInStack().Should().Be(expectedStackSize);
            collection.GetObjectsInUse().Should().Be(0);
        }

        [TestMethod]
        public void TestMinimizeDummy()
        {
            var collection = CreateMockCollection();

            const int size = PoolCollectionBase<MockClass>.MIN_STACK_SIZE;

            collection.GetCurrentStackSize().Should().Be(size);
            collection.GetObjectsInStack().Should().Be(size);
            collection.GetObjectsInUse().Should().Be(0);

            collection.MinimizePool();

            collection.GetCurrentStackSize().Should().Be(size);
            collection.GetObjectsInStack().Should().Be(size);
            collection.GetObjectsInUse().Should().Be(0);
        }

        [TestMethod]
        public void TestTakeReturnMinimize()
        {
            // take batch of objects -> return all -> minimize
            // should have min stack size, full stack, 0 in use

            var collection = CreateMockCollection();

            const int sizeStart = PoolCollectionBase<MockClass>.MIN_STACK_SIZE;
            const int sizeMax = PoolCollectionBase<MockClass>.MAX_STACK_SIZE;

            collection.GetCurrentStackSize().Should().Be(sizeStart);

            List<MockClass> objects = new List<MockClass>();

            for (int i = 0; i < 10000; i++)
            {
                var obj = collection.Take();

                objects.Add(obj);
            }

            collection.GetCurrentStackSize().Should().Be(sizeMax);

            for (int i = objects.Count - 1; i >= 0; i--)
            {
                var obj = objects[i];

                collection.Return(ref obj);
            }

            collection.GetCurrentStackSize().Should().Be(sizeMax);

            collection.MinimizePool();

            collection.GetCurrentStackSize().Should().Be(sizeStart);
        }

        [TestMethod]
        public void TestTakeReturnPartMinimize()
        {
            // take batch of objects -> return some of them -> minimize
            // should have proper stack size, objects in stack, objects in use

            var collection = CreateMockCollection();

            const int sizeStart = PoolCollectionBase<MockClass>.MIN_STACK_SIZE;
            const int sizeMax = PoolCollectionBase<MockClass>.MAX_STACK_SIZE;

            collection.GetCurrentStackSize().Should().Be(sizeStart);

            Stack<MockClass> objects = new Stack<MockClass>();

            for (int i = 0; i < 10000; i++)
            {
                var obj = collection.Take();

                objects.Push(obj);
            }

            collection.GetCurrentStackSize().Should().Be(sizeMax);

            for (int i = 0; i < 256; i++)
            {
                var obj = objects.Pop();

                collection.Return(ref obj);
            }

            collection.MinimizePool();

            const int expectedSize = 272;

            collection.GetCurrentStackSize().Should().Be(expectedSize);
        }

        PoolCollectionBase<MockClass> CreateMockCollection()
        {
            var collection = new GenericPoolCollection<MockClass>(PoolCollectionBase<MockClass>.MIN_STACK_SIZE);

            collection.Initialize();

            return collection;
        }

        class MockClass { }
    }
}
