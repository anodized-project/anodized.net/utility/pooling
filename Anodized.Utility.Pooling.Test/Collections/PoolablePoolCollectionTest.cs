using System;
using System.Collections.Generic;
using System.Text;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Anodized.Utility.Pooling.Test.Collections
{
    [TestClass]
    public class PoolablePoolCollectionTest
    {

        [TestMethod]
        public void TestGet()
        {
            var p = Pool.Get<TestPoolable>();

            p.OnTakeCallsCount.Should().Be(1);
            p.OnReturnCallsCount.Should().Be(0);
        }

        [TestMethod]
        public void TestReturn()
        {
            var p = Pool.Get<TestPoolable>();

            Pool.Free(ref p);

            Assert.IsNull(p);

            p = Pool.Get<TestPoolable>();
            p.OnReturnCallsCount.Should().Be(1);
        }

        class TestPoolable : IPoolable
        {
            public int OnTakeCallsCount, OnReturnCallsCount;

            public void OnTake()
            {
                OnTakeCallsCount++;
            }

            public void OnReturn()
            {
                OnReturnCallsCount++;
            }
        }
    }
}
