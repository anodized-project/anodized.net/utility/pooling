using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

using Anodized.Utility.Pooling.Collections;
using Anodized.Utility.Pooling.Common;

using FluentAssertions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Anodized.Utility.Pooling.Test
{
    [TestClass]
    public class PoolTest
    {
        [TestMethod]
        public void TestGetSimple()
        {
            var empty = Pool.Get<Empty>();

            Assert.IsNotNull(empty);
            empty.Should().BeOfType(typeof(Empty));
        }

        [TestMethod]
        public void TestGetLoop()
        {
            Empty[] arr = new Empty[10000];

            for (var i = 0; i < arr.Length; i++)
            {
                arr[i] = Pool.Get<Empty>();
            }

            CollectionAssert.AllItemsAreNotNull(arr);
            ObjectPool<Empty>.Collection.GetCurrentStackSize().Should()
                             .Be(PoolCollectionBase<Empty>.MAX_STACK_SIZE);
        }

        [TestMethod]
        public void TestGetReturnSimple()
        {
            var empty = Pool.Get<Empty>();

            Assert.IsNotNull(empty);
            Pool.Free(ref empty);
            Assert.IsNull(empty);
        }

        [TestMethod]
        public void TestGetReturnLoopSimple()
        {
            Empty[] arr = new Empty[10000];

            for (var i = 0; i < arr.Length; i++)
            {
                arr[i] = Pool.Get<Empty>();
            }

            CollectionAssert.AllItemsAreNotNull(arr);

            for (var i = 0; i < arr.Length; i++)
            {
                var item = arr[i];

                Pool.Free(ref item);

                arr[i] = item;
            }

            for (var i = 0; i < arr.Length; i++)
            {
                Assert.IsNull(arr[i]);
            }
        }

        [TestMethod]
        public void TestGetLists()
        {
            List<object> lists = Pool.GetList<object>();

            lists.Add(Pool.GetList<string>());
            lists.Add(Pool.GetList<StringBuilder>());
            lists.Add(Pool.GetList<List<List<object>>>());

            Assert.IsNotNull(lists);
            CollectionAssert.AllItemsAreNotNull(lists);
            lists.Should().AllBeAssignableTo<IList>();
        }

        [TestMethod]
        public void TestGetDictionariesHashSets()
        {
            HashSet<object> items = Pool.GetHashSet<object>();
            Assert.IsNotNull(items);

            items.Add(Pool.GetDictionary<int, string>());
            items.Add(Pool.GetHashSet<List<Dictionary<KeyValuePair<string, object>, Queue>>>());

            items.Should().NotContain(i => i == null);
            items.Should().AllBeAssignableTo<IEnumerable>();
        }

        [TestMethod]
        public void TestGetReturnStringBuilder()
        {
            StringBuilder builder = Pool.GetStringBuilder();

            Assert.IsNotNull(builder);

            builder.Append("Some text content which should be deleted");

            builder.Length.Should().NotBe(0);

            Pool.Free(ref builder);

            Assert.IsNull(builder);
            
            StringBuilder builder2 = Pool.GetStringBuilder();

            Assert.IsNotNull(builder2);

            builder2.Length.Should().Be(0);
        }

        class Empty { }
    }
}
