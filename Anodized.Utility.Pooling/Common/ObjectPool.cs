using System;
using System.Linq;

using Anodized.Utility.Pooling.Collections;

namespace Anodized.Utility.Pooling.Common
{
    static class ObjectPool<T> where T : class,new()
    {
        public static readonly IPoolCollection<T> Collection;

        public static readonly Type CollectionType = typeof(T); 

        static ObjectPool()
        {
            if (TypeDefs.IPoolableType.IsAssignableFrom(CollectionType))
            {
                Type pcollGen = TypeDefs.PoolablePoolCollectionType.MakeGenericType(CollectionType);

                Collection = (IPoolCollection<T>)Activator.CreateInstance(pcollGen);
                Collection.Initialize();
                return;
            }

            Type icol = CollectionType.GetInterfaces().FirstOrDefault(
                i => i.IsGenericType && i.GetGenericTypeDefinition() == TypeDefs.ICollectionType
            );

            if (icol != null)
            {
                Type holdingType = icol.GenericTypeArguments[0];

                Type icolGen = TypeDefs.CollectionPoolCollectionType.MakeGenericType(CollectionType, holdingType);

                Collection = (IPoolCollection<T>)Activator.CreateInstance(icolGen);
                Collection.Initialize();
                return;
            }

            Collection = new GenericPoolCollection<T>();
            Collection.Initialize();
        }
    }
}
