﻿using System;
using System.Collections.Generic;
using System.Text;

using Anodized.Utility.Pooling.Collections;

namespace Anodized.Utility.Pooling.Common
{
    static class TypeDefs
    {
        public static readonly Type IPoolCollectionType          = typeof(IPoolCollection<>);
        public static readonly Type PoolCollectionBaseType       = typeof(PoolCollectionBase<>);
        public static readonly Type PoolablePoolCollectionType   = typeof(PoolablePoolCollection<>);
        public static readonly Type GenericPoolCollectionType    = typeof(GenericPoolCollection<>);
        public static readonly Type CollectionPoolCollectionType = typeof(CollectionPoolCollection<,>);
        public static readonly Type IPoolableType                = typeof(IPoolable);
        public static readonly Type ICollectionType              = typeof(ICollection<>);
    }
}
