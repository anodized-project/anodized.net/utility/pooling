using System;
using System.Collections.Generic;
using System.Text;

using Anodized.Utility.Pooling.Common;

namespace Anodized.Utility.Pooling
{
    /// <summary>
    /// General purpose interface to access default pools
    /// </summary>
    public static class Pool
    {
        public static T Get<T>() where T : class, new()
        {
            return ObjectPool<T>.Collection.Take();
        }

        public static List<T> GetList<T>()
        {
            return Get<List<T>>();
        }

        public static Dictionary<TKey, TValue> GetDictionary<TKey, TValue>()
        {
            return Get<Dictionary<TKey, TValue>>();
        }

        public static HashSet<T> GetHashSet<T>()
        {
            return Get<HashSet<T>>();
        }

        public static StringBuilder GetStringBuilder()
        {
            return Get<StringBuilder>();
        }

        public static void Free<T>(ref T obj) where T : class, new()
        {
            ObjectPool<T>.Collection.Return(ref obj);
        }

        public static void Free(ref StringBuilder stringBuilder)
        {
            stringBuilder.Clear();
            Free<StringBuilder>(ref stringBuilder);
        }

        public static void MinimizePool<T>() where T : class, new()
        {
            ObjectPool<T>.Collection.MinimizePool();
        }
    }
}
