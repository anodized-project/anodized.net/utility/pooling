namespace Anodized.Utility.Pooling
{
    /// <summary>
    /// Object implementing this interface may contain custom logic executed the moment object is taken from / returned to pool
    /// </summary>
    public interface IPoolable
    {
        /// <summary>
        /// Method called while object is taken from pool
        /// </summary>
        void OnTake();

        /// <summary>
        /// Method called while object is being returned to pool
        /// </summary>
        void OnReturn();
    }
}
