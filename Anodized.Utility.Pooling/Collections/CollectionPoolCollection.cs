using System;
using System.Collections.Generic;
using System.Text;

using Anodized.Utility.Pooling.Common;

namespace Anodized.Utility.Pooling.Collections
{
    class CollectionPoolCollection<TCollection, THoldable> : GenericPoolCollection<TCollection> where TCollection : class, ICollection<THoldable>, new()
    {
        public override void Return(ref TCollection obj)
        {
            obj.Clear();
            base.Return(ref obj);
        }
    }
}
