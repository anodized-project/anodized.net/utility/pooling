using System;
using System.Collections.Generic;
using System.Text;

using Anodized.Utility.Pooling.Common;

namespace Anodized.Utility.Pooling.Collections
{
    class PoolablePoolCollection<T> : GenericPoolCollection<T> where T : class, IPoolable,new()
    {
        public override T Take()
        {
            T obj = base.Take();
            obj.OnTake();
            return obj;
        }

        public override void Return(ref T obj)
        {
            obj.OnReturn();
            base.Return(ref obj);
        }
    }
}
