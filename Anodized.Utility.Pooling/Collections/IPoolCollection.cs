using System;

namespace Anodized.Utility.Pooling.Collections
{
    /// <summary>
    /// Represents single-type collection which provides methods to get and return items
    /// as well as to keep track of pool size
    /// </summary>
    /// <typeparam name="T"></typeparam>
    interface IPoolCollection<T> where T : class
    {
        /// <summary>
        /// Called once in the default pool implementation to populate pool with objects
        /// </summary>
        void Initialize();

        T Take();

        void Return(ref T obj);

        void MinimizePool();

        int GetCurrentStackSize();

        int GetObjectsInStack();

        int GetObjectsInUse();

        public struct Element
        {
            T _obj;

            public bool IsWrapped => _obj != null;

            public void Wrap(T obj)
            {
#if STRICT_WRAP_UNWRAP || DEBUG
                if(IsWrapped)
                {
                    throw new InvalidOperationException("Element already has wrapped object");
                }
#endif

                _obj = obj;
            }

            public T UnWrap()
            {
#if STRICT_WRAP_UNWRAP || DEBUG
                if(!IsWrapped)
                {
                    throw new InvalidOperationException("Element does not contain wrapped object");
                }
#endif
                T obj = _obj;
                _obj = null;
                return obj;
            }
        }
    }
}
