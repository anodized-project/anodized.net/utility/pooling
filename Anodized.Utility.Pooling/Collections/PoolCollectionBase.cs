using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;

using Anodized.Utility.Pooling.Common;

namespace Anodized.Utility.Pooling.Collections
{
    abstract class PoolCollectionBase<T> : IPoolCollection<T> where T : class
    {
        public const int MIN_STACK_SIZE    = 16,
                         STACK_RESIZE_STEP = 16,
                         MAX_STACK_SIZE    = 512;

        protected IPoolCollection<T>.Element[] stack;

        protected volatile int objectsInStack,
                               objectsInUse,
                               stackSize;

        protected PoolCollectionBase() : this(MIN_STACK_SIZE) { }

        protected PoolCollectionBase(int initStackSize)
        {
            stackSize = initStackSize;
            stack = new IPoolCollection<T>.Element[initStackSize];
        }

        public virtual void Initialize()
        {
            for (int i = 0; i < stackSize; i++)
            {
                T obj = CreateObject();

                stack[i].Wrap(obj);
            }

            objectsInStack = stackSize;
        }
        
        public virtual void Return(ref T obj)
        {
            // check if should put object into stack or discard it (if stack is full)

            if (objectsInStack < stackSize)
            {
                // return into pool
                stack[objectsInStack].Wrap(obj);

                // ReSharper disable ConvertToCompoundAssignment
                objectsInStack = objectsInStack + 1;
                objectsInUse = objectsInUse - 1;
                // ReSharper restore ConvertToCompoundAssignment
            }
            
            // discard object
            obj = null;
        }

        public virtual T Take()
        {
            T obj;
            if (objectsInStack > 1)
            {
                obj = stack[objectsInStack - 1].UnWrap();

                // ReSharper disable ConvertToCompoundAssignment
                objectsInStack = objectsInStack - 1;
                objectsInUse = objectsInUse + 1;
                // ReSharper restore ConvertToCompoundAssignment

                return obj;
            }

            if (objectsInStack == 0)
            {
                return CreateObject();
            }

            // objectsInStack == 1 path
            obj = stack[0].UnWrap();

            objectsInUse = stackSize; // just removing unnecessary addition, meaning is '+= 1'

            if (stackSize == MAX_STACK_SIZE)
            {
                objectsInStack = 0;
                return obj;
            }

            int expSize = stackSize + STACK_RESIZE_STEP;
            objectsInStack = STACK_RESIZE_STEP;

            stack = new IPoolCollection<T>.Element[expSize];

            for (int i = 0; i < STACK_RESIZE_STEP; i++)
            {
                stack[i].Wrap(CreateObject());
            }

            stackSize = expSize;

            return obj;
        }

        public virtual void MinimizePool()
        {
            if (stackSize == MIN_STACK_SIZE)
            {
                return;
            }

            IPoolCollection<T>.Element[] newStack;

            if (objectsInUse == 0)
            {
                // minimize to the smallest

                newStack = new IPoolCollection<T>.Element[MIN_STACK_SIZE];

                for (int i = 0; i < MIN_STACK_SIZE; i++)
                {
                    newStack[i] = stack[i];
                }

                objectsInStack = MIN_STACK_SIZE;
                stack = newStack;
                stackSize = MIN_STACK_SIZE;
                return;
            }

            bool flag = objectsInUse % STACK_RESIZE_STEP == 0;

            int min = Math.Max(MIN_STACK_SIZE, objectsInUse);

            int mod = min % STACK_RESIZE_STEP;
            int num = min - mod;
            int newStackSize;

            if (flag)
            {
                newStackSize = num + STACK_RESIZE_STEP;
            }
            else
            {
                newStackSize = num;
            }

            newStack = new IPoolCollection<T>.Element[newStackSize];

            int fill = newStackSize - objectsInUse;

            if (objectsInStack >= fill)
            {
                for (int i = 0; i < fill; i++)
                {
                    newStack[i] = stack[i];
                }
            }
            else
            {
                int diff = fill - objectsInStack;

                for (int i = 0; i < objectsInStack; i++)
                {
                    newStack[i] = stack[i];
                }

                for (int i = objectsInStack; i < diff + objectsInStack; i++)
                {
                    newStack[i].Wrap(CreateObject());
                }
            }

            objectsInStack = fill;
            stackSize = newStackSize;
            stack = newStack;
        }

        public int GetCurrentStackSize()
        {
            return stackSize;
        }

        public int GetObjectsInStack()
        {
            return objectsInStack;
        }

        public int GetObjectsInUse()
        {
            return objectsInUse;
        }

        protected abstract T CreateObject();
    }
}
