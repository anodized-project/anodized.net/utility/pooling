using System;
using System.Collections.Generic;
using System.Text;

using Anodized.Utility.Pooling.Common;

namespace Anodized.Utility.Pooling.Collections
{
    class GenericPoolCollection<T> : PoolCollectionBase<T> where T : class, new()
    {
        public GenericPoolCollection()
        {
        }

        public GenericPoolCollection(int initStackSize) : base(initStackSize)
        {
        }

        protected override T CreateObject()
        {
            return new T();
        }
    }
}
